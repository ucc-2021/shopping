INSERT INTO product (description,name,price) VALUES ('leche','leche',3500.0);
INSERT INTO product (description,name,price) VALUES ('yogurt','yogurt',2000.0);
INSERT INTO product (description,name,price) VALUES ('leche de almendras','leche de almendras',2000.0);
INSERT INTO product (description,name,price) VALUES ('yogurt casero','yogurt casero',5000.0);
INSERT INTO product (description,name,price) VALUES ('atun','atun',6000.0);
INSERT INTO product (description,name,price) VALUES ('libra de arroz','arroz',2500.0);
INSERT INTO product (description,name,price) VALUES ('salchichas zenu','salchichas',4000.0);
INSERT INTO product (description,name,price) VALUES ('rancheras','salchichas rancheras',4000.0);
INSERT INTO product (description,name,price) VALUES ('salchichon','salchichon',5000.0);

INSERT INTO category (description,name) VALUES ('lacteos naturales','lacteos naturales');
INSERT INTO category (description,name) VALUES ('lacteos procesados','lacteos procesados');

INSERT INTO product_category (id_category,id_product) VALUES (1,1);
INSERT INTO product_category (id_category,id_product) VALUES (2,2);
INSERT INTO product_category (id_category,id_product) VALUES (1,3);
INSERT INTO product_category (id_category,id_product) VALUES (1,4);
INSERT INTO product_category (id_category,id_product) VALUES (2,4);