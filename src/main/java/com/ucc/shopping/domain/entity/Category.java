package com.ucc.shopping.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "category", schema = "shopping")
public class Category {
  /**
   * CAPA DE ACCESO A DATOS, ENTIDADES (Tablas), ORM - Object Relational Mapping
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "name", nullable = false, length = 100)
  private String name;

  @Column(name = "description", nullable = false, length = 300)
  private String description;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
