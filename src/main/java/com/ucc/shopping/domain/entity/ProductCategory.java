package com.ucc.shopping.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "product_category", schema = "shopping",
  uniqueConstraints = {
      @UniqueConstraint(
          name = "product_category_UN", columnNames = {"id_category", "id_product"})
  })
public class ProductCategory {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "id_category", nullable = false)
  private Long idCategory;

  @JoinColumn(name = "id_category", referencedColumnName = "id", updatable = false, insertable = false)
  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  @JsonIgnore
  private Category category;

  @Column(name = "id_product", nullable = false)
  private Long idProduct;

  @JoinColumn(name = "id_product", referencedColumnName = "id", updatable = false, insertable = false)
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JsonIgnore
  private Product product;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getIdCategory() {
    return idCategory;
  }

  public void setIdCategory(Long idCategory) {
    this.idCategory = idCategory;
  }

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public Long getIdProduct() {
    return idProduct;
  }

  public void setIdProduct(Long idProduct) {
    this.idProduct = idProduct;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }
}
