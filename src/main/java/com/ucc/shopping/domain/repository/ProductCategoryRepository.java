package com.ucc.shopping.domain.repository;

import com.ucc.shopping.domain.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {

    List<ProductCategory> findAllByIdProduct(Long idProduct);
}
