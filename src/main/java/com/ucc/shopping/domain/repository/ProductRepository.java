package com.ucc.shopping.domain.repository;

import com.ucc.shopping.domain.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author danny
 * @project shopping
 * @class ProductRepository
 * @date 06/03/2021
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findAllByOrderByName();

    // select * from product p order by p.name
    @Query(value = "select pd from Product pd order by pd.name")
    List<Product> getAllOrderByName();

    List<Product> findByNameContains(String name);

    @Query(value = "select pd from Product pd where pd.name like :name")
    List<Product> getAllByNameIsLike(@Param("name") String name);

    @Query(value = "select pd from Product pd where pd.name = :name")
    Optional<Product> findByName(@Param("name") String name);

    /**
     * select p.* from product p
     * inner join product_category pc on (p.id = pc.id_product)
     * inner join category c on (c.id = pc.id_category)
     * where c.name = 'lacteos naturales';
     */
    // utiliza hints directamente en base de datos
    @Query(value = "select p from Product p " +
            "inner join ProductCategory pc on (p.id = pc.idProduct)" +
            "inner join Category c on (c.id = pc.idCategory) " +
            "where c.name = :categoryName order by p.price desc ")
    List<Product> getAllByCategoryName(@Param("categoryName") String categoryName);

    @Query(value = "select sum(p.price) from Product p " +
            "inner join ProductCategory pc on (p.id = pc.idProduct)" +
            "inner join Category c on (c.id = pc.idCategory) " +
            "where c.name = :categoryName order by p.price desc ")
    Optional<Integer> sumProductByCategoryName(@Param("categoryName") String categoryName);

    // utd hizo el query, utd se encarga del rendimiento
    @Query(value = "select p.* from product p " +
            "inner join product_category pc on (p.id = pc.id_product) " +
            "inner join category c on (c.id = pc.id_category) " +
            "where c.name = :categoryName", nativeQuery = true)
    List<Product> getAllByCategoryNameNative(@Param("categoryName") String categoryName);
}
