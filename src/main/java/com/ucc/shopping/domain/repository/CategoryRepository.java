package com.ucc.shopping.domain.repository;

import com.ucc.shopping.domain.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
  /**
   * CAPA DE ACCESO A DATOS - REPOSITORIO DE CONSULTAS - DAO (Data Access Object)
   */

  // CRUD hasdhasdhasdahsdhas
  @Override
  List<Category> findAll();

  Optional<Category> findById(Long id);

  Category save(Category category);

  void deleteById(Long id);

  // averiguar como desde un repositorio puedo crear un query


}
