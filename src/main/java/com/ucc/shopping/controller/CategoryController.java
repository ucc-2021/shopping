package com.ucc.shopping.controller;

import com.ucc.shopping.domain.entity.Category;
import com.ucc.shopping.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danny
 * @project shopping
 * @class CategoryController
 * @date 05/03/2021
 */
// este es el API, lo expuesto -> interfaz de comunicación
@RestController // vamos a exponer unos endpoints
public class CategoryController {

    /**
     * CAPA DE PRESENTACIÓN - CONTROLADORES TIPO REST - ENDPOINTS
     */
    @Autowired
    private CategoryService categoryService;

    @GetMapping("categories")
    public ResponseEntity<List<Category>> findAll() {
        return ResponseEntity.ok(categoryService.findAll());
    }

}
