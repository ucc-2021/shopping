package com.ucc.shopping.service;

import com.ucc.shopping.domain.dto.ProductDTO;
import com.ucc.shopping.domain.dto.ProductForm;
import com.ucc.shopping.domain.entity.Product;

import java.util.List;

/**
 * @author danny
 * @project shopping
 * @class ProductService
 * @date 06/03/2021
 */
public interface ProductService {

    List<Product> getAllProductsOrderByName();

    List<Product> getAllByNameIsLike(String name);

    Product getById(Long id);

    boolean deleteById(Long id);

    Product getByName(String name);

    List<Product> getAllProductsByCategoryName(String categoryName);

    Integer sumProductByCategoryName(String categoryName);

    ProductDTO save(ProductForm form);
}
